# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Berny <berny@bernhard-marx.de>, 2022
# Felix <felix.com@gmx.com>, 2022
# Thomas Lauterbach <lauterbachthomas@googlemail.com>, 2022
# Mathias Behrle <mathiasb@m9s.biz>, 2022
# Marvin Haschker <marvin@haschker.me>, 2022
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-21 08:49+0000\n"
"PO-Revision-Date: 2022-02-03 10:12+0000\n"
"Last-Translator: Marvin Haschker <marvin@haschker.me>, 2022\n"
"Language-Team: German (Germany) (https://www.transifex.com/rosarior/teams/13584/de_DE/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de_DE\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:16
msgid "Databases"
msgstr ""

#: literals.py:12
msgid ""
"Your database backend is set to use SQLite. SQLite should only be used for "
"development and testing, not for production."
msgstr ""
"Sie benutzen SQLite als Datenbank-Backend. SQLite sollte nur für "
"Entwicklungs- und Testzwecke verwendet werden, jedoch nicht in "
"Produktivumgebungen."

#: model_mixins.py:18
msgid "The dotted Python path to the backend class."
msgstr "Der punktierte Pythonpfad zur Backendklasse."

#: model_mixins.py:19
msgid "Backend path"
msgstr "Backendpfad"

#: model_mixins.py:23
msgid "JSON encoded data for the backend class."
msgstr "JSON kodierte Daten für die Backend Klasse."

#: model_mixins.py:24
msgid "Backend data"
msgstr "Backenddaten"

#: model_mixins.py:72
msgid "Backend"
msgstr "Backend"

#: model_mixins.py:73
msgid "The backend class for this entry."
msgstr "Die Backend-Klasse für diesen Eintrag."

#: templates/databases/app/viewport.html:11
msgid "Warning"
msgstr "Warnung"
